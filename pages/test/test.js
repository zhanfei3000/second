//获取应用实例
var app = getApp();
var common = require("../../common/common.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {

    name:"tangcaiye",
    pass:null,
    id:8,
    bol:true,
    arr:[1,2,3,4,5],
    obj1:{
      a:6,
      b:7
    }
  },
  outterTap:function(e){
    console.log(e);
    console.log("触发里 outter 的tap事件");

  },
  middleTap:function(e){
    console.log(e);
    console.log("触发里 middle 的tap事件");

  },
  innerTap:function(e){
    console.log(e);
    console.log("触发里inner 的tap事件");
  },
  show:function(){
    // var bol = this.data.bol;
    this.setData({

      // bol:!bol
      bol:! this.data.bol
    });
  },
  say:function(){
    common.sayHello(this.data.name);
  },
  toIndex: function(){
   wx.navigateTo({
     url:'../navi/navi',
     success:function(){
      console.log("跳转到navi成功")
     }
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.setData({
      pass:app.globalData.pass
    });
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log("这是测试页里的onShow方法");
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onLoad: function (options) {

    this.setData({
      pass:app.globalData.pass
    });
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})