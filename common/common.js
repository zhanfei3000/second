// common.js
require("a.js");
console.log("我是 common.js");
function sayHello(name) {
  console.log(`Hello ` + name + `!`)
}
// function sayGoodbye(name) {
//   console.log(`Goodbye ${name} !`)
// }
// module.exports ＝ ｛
// 	sayHello:sayHello
// ｝
exports.sayHello = sayHello;
// module.exports.sayHello = sayHello;
// exports.sayGoodbye = sayGoodbye
// 
// 
